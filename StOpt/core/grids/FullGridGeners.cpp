#include "StOpt/core/grids/FullGridGeners.h"
// add include for all derived classes
#include "StOpt/core/grids/RegularLegendreGridDerivedGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridDerivedGeners.h"
#include "StOpt/core/grids/RegularSpaceGridDerivedGeners.h"

// Register all wrappers
SerializationFactoryForFullGrid::SerializationFactoryForFullGrid()
{
    this->registerWrapper<RegularLegendreGridDerivedGeners>();
    this->registerWrapper<GeneralSpaceGridDerivedGeners>();
    this->registerWrapper<RegularSpaceGridDerivedGeners>();
}
