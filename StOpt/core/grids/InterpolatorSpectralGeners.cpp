#include "StOpt/core/grids/InterpolatorSpectralGeners.h"
// add include for all derived classes
#include "StOpt/core/grids/LinearInterpolatorSpectralGeners.h"
#include "StOpt/core/grids/LegendreInterpolatorSpectralGeners.h"
#include "StOpt/core/grids/SparseInterpolatorSpectralGeners.h"

// Register all wrappers
SerializationFactoryForInterpolatorSpectral::SerializationFactoryForInterpolatorSpectral()
{
    this->registerWrapper<LinearInterpolatorSpectralGeners>();
    this->registerWrapper<LegendreInterpolatorSpectralGeners>();
    this->registerWrapper<SparseInterpolatorSpectralGeners>();
}
