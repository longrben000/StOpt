// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <boost/version.hpp>
#include <Eigen/Core>
#include "StOpt/core/utils/version.h"

namespace StOpt
{
std::string getStOptVersion()
{
    std::stringstream stOpt_version;

    stOpt_version << "StOpt version "
                  << STOPT_MAJOR_VERSION << "."
                  << STOPT_MINOR_VERSION << " \n";

    stOpt_version << "Boost version " << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << "." << BOOST_VERSION % 100 << "\n";

    stOpt_version << "Eigen version 3." << EIGEN_MAJOR_VERSION << "." << EIGEN_MINOR_VERSION << "\n";

    return stOpt_version.str();
}
}
