// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef PAYOFFBASE_H
#define PAYOFFBASE_H
#include <functional>
#include <Eigen/Dense>

/** \file  PayOffBase.h
 *  \brief used to map python to function used for pay off
 *  \brief base class for pay off
 * \author Xavier Warin
 */

namespace StOpt
{
/// \class PayOffBase PayOffBase.h
/// Base class for pay off for python binding
class PayOffBase
{
public :
    PayOffBase() {}
    // virtual ~PayOffBase() {}
    /// \brief get back the function pay off
    virtual std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>  getFunction() const = 0;
} ;
}
#endif /* PAYOFFBASE_H */
