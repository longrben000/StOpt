
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include "StOpt/regression/ContinuationValue.h"
#include "StOpt/core/grids/SpaceGrid.h"


namespace py = pybind11;


class BaseA
{
public:
    BaseA()  {}
    virtual double get()  const = 0;
};

class DerA : public BaseA
{
    double m_x ;
public:
    DerA(const double &x) : m_x(x) {}
    virtual double get()  const
    {
        return 2. ;
    }
};

// wrapper
class PyBaseA : public BaseA
{
public:
    using BaseA::BaseA;
    double  get() const override
    {
        PYBIND11_OVERLOAD_PURE(double, BaseA, get);
    }
};

class PyDerA : public DerA
{
public:
    using DerA::DerA;
    double  get() const override
    {
        PYBIND11_OVERLOAD_PURE(double, DerA, get);
    }
};


PYBIND11_MODULE(pyBind, m)
{
    pybind11::class_< BaseA, PyBaseA >(m, "BaseA")
    .def(py::init<>())
    .def("get", &BaseA::get)
    ;

    pybind11::class_< DerA, std::shared_ptr<DerA>, PyDerA, BaseA >(m, "DerA")
    // .def(py::init< double >())
    .def(py::init([](const py::list & p_x)
    {

        return new DerA(p_x[0].cast<double>()) ;
    }
                 ))

    .def("get", &DerA::get)
    ;

}

