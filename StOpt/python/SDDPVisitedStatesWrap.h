// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SDDPVISITEDSTATESWRAP_H
#define SDDPVISITEDSTATESWRAP_H
#include <memory>
#include <Eigen/Dense>
#include "StOpt/sddp/SDDPVisitedStates.h"
#include "StOpt/sddp/LocalLinearRegressionForSDDP.h"

/// wrapper for SDDPVisitedStates
///*****************************
class  SDDPVisitedStatesWrap:  public StOpt::SDDPVisitedStates
{

public:

    SDDPVisitedStatesWrap(): StOpt::SDDPVisitedStates() {}

    void addVisitedState(const std::shared_ptr< Eigen::ArrayXd > &p_state, const Eigen::ArrayXd &p_particle, const StOpt::LocalLinearRegressionForSDDP   &p_regressor)
    {
        StOpt::SDDPVisitedStates::addVisitedState(p_state, p_particle, p_regressor);
    }

    void addVisitedStateForAll(const std::shared_ptr< Eigen::ArrayXd > &p_state,  const StOpt::LocalLinearRegressionForSDDP   &p_regressor)
    {
        StOpt::SDDPVisitedStates::addVisitedStateForAll(p_state, p_regressor);
    }
};

#endif
