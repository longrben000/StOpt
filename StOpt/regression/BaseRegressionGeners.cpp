#include "StOpt/regression/BaseRegressionGeners.h"
// add include for all derived classes
#include "StOpt/regression/LocalConstRegressionGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "StOpt/regression/LocalSameSizeConstRegressionGeners.h"
#include "StOpt/regression/LocalSameSizeLinearRegressionGeners.h"
#include "StOpt/regression/LocalKMeansRegressionGeners.h"
#include "StOpt/regression/LocalLinearDiscrLastDimRegressionGeners.h"
#include "StOpt/regression/LocalConstDiscrLastDimRegressionGeners.h"
#include "StOpt/regression/SparseRegressionGeners.h"
#include "StOpt/regression/GlobalRegressionGeners.h"
#include "StOpt/regression/LocalGridKernelRegressionGeners.h"
#include "StOpt/regression/LaplacianConstKernelRegressionGeners.h"
#include "StOpt/regression/LaplacianLinearKernelRegressionGeners.h"
#include "StOpt/regression/LaplacianGridKernelRegressionGeners.h"

// Register all wrappers
SerializationFactoryForBaseRegression::SerializationFactoryForBaseRegression()
{
    registerWrapper<LocalConstRegressionGeners>();
    registerWrapper<LocalLinearRegressionGeners>();
    registerWrapper<LocalConstDiscrLastDimRegressionGeners>();
    registerWrapper<LocalLinearDiscrLastDimRegressionGeners>();
    registerWrapper<LocalKMeansRegressionGeners>();
    registerWrapper<LocalSameSizeConstRegressionGeners>();
    registerWrapper<LocalSameSizeLinearRegressionGeners>();
    registerWrapper<SparseRegressionGeners>();
    registerWrapper<GlobalRegressionGeners<StOpt::Hermite> >();
    registerWrapper<GlobalRegressionGeners<StOpt::Canonical> >();
    registerWrapper<GlobalRegressionGeners<StOpt::Tchebychev> >();
    registerWrapper<LocalGridKernelRegressionGeners>();
    registerWrapper<LaplacianConstKernelRegressionGeners>();
    registerWrapper<LaplacianLinearKernelRegressionGeners>();
    registerWrapper<LaplacianGridKernelRegressionGeners>();
}
