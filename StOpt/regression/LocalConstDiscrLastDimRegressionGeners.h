// Copyright (C) 2021 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALCONSTDISCRLASTDIMREGRESSIONGENERS_H
#define LOCALCONSTDISCRLASTDIMREGRESSIONGENERS_H
#include <geners/AbsReaderWriter.hh>
#include "StOpt/regression/LocalConstDiscrLastDimRegression.h"
#include "StOpt/regression/BaseRegressionGeners.h"

/** \file LocalConstDiscrLastDimRegressionGeners.h
 * \brief Define non intrusive  serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class LocalConstDiscrLastDimRegression
// Note publication of LocalConstDiscrLastDimRegression as "wrapped_type".
struct LocalConstDiscrLastDimRegressionGeners: public gs::AbsReaderWriter<StOpt::BaseRegression>
{
    typedef StOpt::BaseRegression wrapped_base;
    typedef StOpt::LocalConstDiscrLastDimRegression wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id forLocalConstDiscrLastDimRegression  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::LocalConstDiscrLastDimRegression, 1)
gs_declare_type_external(StOpt::LocalConstDiscrLastDimRegression)
gs_associate_serialization_factory(StOpt::LocalConstDiscrLastDimRegression, StaticSerializationFactoryForBaseRegression)

#endif  /* LOCALCONSTDISCRLASTDIMREGRESSIONGENERS_H */
