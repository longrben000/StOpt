// Copyright (C) 2016 2021 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef MESHCALCULATIONLOCALREGRESSION_H
#define MESHCALCULATIONLOCALREGRESSION_H
#include <array>
#include <vector>
#include <memory>
#include <Eigen/Dense>

/** \file   meshCalculationLocalRegression.h
 *  \brief  Permits to calculate the adapted mesh to particle distribution
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \addtogroup LocalLinear
 * @{
 */

/// \brief Calculate the local mesh
/// \param  p_particles    particles used for the meshes.
///                        First dimension  : dimension of the problem,
///                        second dimension : the  number of particles
/// \param p_nbMesh       number of meshes in each direction
/// \param p_simToCell    for each simulation, gives its global position in the Cartesian meshing
/// \param p_mesh         describe the mesh generated (first dimension is  the dimension of the problem, the second  dimension is the number of mesh)
///                       For each cell and dimension gives the coordinates min and max of the cell
/// \param p_mesh1D       second representation of the discretization per dimension (conform mesh)
void meshCalculationLocalRegression(const Eigen::ArrayXXd &p_particles, const Eigen::ArrayXi   &p_nbMesh,
                                    Eigen::ArrayXi &p_simToCell, Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic > &p_mesh,
                                    std::vector< std::shared_ptr<Eigen::ArrayXd > >   &p_mesh1D);



/// \brief Calculate the local mesh : in this version last dimension has discrete values that are gathered
/// \param  p_particles    particles used for the meshes.
///                        First dimension  : dimension of the problem,
///                        second dimension : the  number of particles
/// \param p_nbMesh       number of meshes in each direction
/// \param p_simToCell    for each simulation, gives its global position in the Cartesian meshing
/// \param p_mesh         describe the mesh generated (first dimension is  the dimension of the problem, the second  dimension is the number of mesh)
///                       For each cell and dimension gives the coordinates min and max of the cell
/// \param p_mesh1D       second representation of the discretization per dimension (conform mesh)
void meshCalculationLocalRegressionDiscrLastDim(const Eigen::ArrayXXd &p_particles, const Eigen::ArrayXi   &p_nbMesh,
        Eigen::ArrayXi &p_simToCell, Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic > &p_mesh,
        std::vector< std::shared_ptr<Eigen::ArrayXd > >   &p_mesh1D);
}
/**@}*/

#endif /*MESHCALCULATIONLOCALREGRESSION_H*/
