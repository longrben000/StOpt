image: $BUILD_IMAGE
# Specific image with all needed dependencies, including python and jupyter.

stages:
  - build
  - test
  - pack
  - build-debug
  - test-debug

# -----------------
# no MPI tests
# -----------------

build:no-mpi-release:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev ninja-build
  script:
    - cmake . -B build -G Ninja
      -DBUILD_MPI=false 
      -DBUILD_PYTHON=true 
      -DBUILD_SDDP=true 
      -DBUILD_TEST=true 
      -DBUILD_BRANCHING=false 
      -DBUILD_DPCUTS=true 
    - cmake --build build --config Release -- -j8
  artifacts:
    when: on_success
    paths:
      - build


tests:no-mpi-release:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:no-mpi-release
  dependencies:
    - build:no-mpi-release
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev xsltproc
  script:
    - export OMP_NUM_THREADS=2
    - cd build
    - ctest -T Test --no-compress-output || status=$? || true
    - if [ -f Testing/TAG ] ; then
    -    xsltproc $CTEST_TO_JUNIT Testing/`head -n 1 < Testing/TAG`/Test.xml > CTestResults.xml || true
    - fi
    - exit $status
  artifacts:
    when: always
    paths:
      - build/Testing/**/Test.xml
      - build/CTestResults.xml
    reports:
      junit:
        - build/CTestResults.xml


python-tests:no-mpi-release:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:no-mpi-release
  dependencies: 
    - build:no-mpi-release
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev
    - pip3 install nose
    - pip3 install matplotlib
  script:
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/build/lib
    - export PYTHONPATH=$PYTHONPATH:$(pwd)/build/lib:test/python/unit:test/python/functional
    - nosetests --exclude=Mpi test/python/unit       --with-xunit --xunit-file=nosetests-unit.xml       --xunit-testsuite-name=unit       || status1=$? || true
    - nosetests --exclude=Mpi test/python/functional --with-xunit --xunit-file=nosetests-functional.xml --xunit-testsuite-name=functional || status2=$? || true
    - if [ $status1 -ne 0 ] || [ $status2 -ne 0 ] ; then
    -    exit 1
    - fi
  artifacts:
    when: always
    reports:
      junit:
        - nosetests-unit.xml
        - nosetests-functional.xml


pack:no-mpi-release:
  stage: pack
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:no-mpi-release
  dependencies: 
    - build:no-mpi-release
  script:
    - cd build
    - cpack -v --config Release
  artifacts:
    when: always
    paths:
      - build/StOpt*


# -----------------
# with MPI tests
# -----------------

build:mpi-release:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH

# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev ninja-build libopenmpi-dev libboost-mpi-dev
  script:
    - cmake . -B build -G Ninja
      -DBUILD_MPI=true 
      -DBUILD_PYTHON=true 
      -DBUILD_SDDP=true 
      -DBUILD_TEST=true 
      -DBUILD_BRANCHING=true 
      -DBUILD_DPCUTS=true 
      -DMPIEXEC_PREFLAGS=--allow-run-as-root
    - cmake --build build --config Release -- -j8
  artifacts:
    when: on_success
    paths:
      - build


tests:mpi-release:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:mpi-release
  dependencies: 
    - build:mpi-release
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev xsltproc libopenmpi-dev libboost-mpi-dev

  script:
    - export OMP_NUM_THREADS=1
    - cd build
    - ctest -T Test --no-compress-output || status=$? || true
    - if [ -f Testing/TAG ] ; then
    -    xsltproc $CTEST_TO_JUNIT Testing/`head -n 1 < Testing/TAG`/Test.xml > CTestResults.xml || true
    - fi
    - exit $status
  artifacts:
    when: always
    paths:
      - build/Testing/**/Test.xml
      - build/CTestResults.xml
    reports:
      junit:
        - build/CTestResults.xml


python-tests:mpi-release:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:mpi-release
  dependencies: 
    - build:mpi-release
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev libopenmpi-dev libboost-mpi-dev
    - pip3 install nose
    - pip3 install matplotlib
    - pip3 install mpi4py
  script:
    - export OMP_NUM_THREADS=1
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/build/lib
    - export PYTHONPATH=$PYTHONPATH:$(pwd)/build/lib:test/python/unit:test/python/functional
    - nosetests test/python/unit       --with-xunit --xunit-file=nosetests-unit.xml       --xunit-testsuite-name=unit       || status1=$? || true
    - nosetests test/python/functional --with-xunit --xunit-file=nosetests-functional.xml --xunit-testsuite-name=functional || status2=$? || true
    - if [ $status1 -ne 0 ] || [ $status2 -ne 0 ] ; then
    -    exit 1
    - fi
  artifacts:
    when: always
    reports:
      junit:
        - nosetests-unit.xml
        - nosetests-functional.xml


pack:mpi-release:
  stage: pack
  rules:
    - if: $CI_COMMIT_BRANCH
  needs:
    - build:mpi-release
  dependencies: 
    - build:mpi-release
  script:
    - cd build
    - cpack -v --config Release
  artifacts:
    when: always
    paths:
      - build/StOpt*


# -----------------
#  clang build
# -----------------
build:clang-mpi-release:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH

# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev ninja-build libopenmpi-dev libboost-mpi-dev clang
  script:
    - cmake . -B build -G Ninja
      -DCMAKE_CXX_COMPILER=/usr/bin/clang++
      -DCMAKE_C_COMPILER=/usr/bin/clang
      -DBUILD_MPI=true 
      -DBUILD_PYTHON=true 
      -DBUILD_SDDP=true 
      -DBUILD_TEST=true 
      -DBUILD_BRANCHING=true 
      -DBUILD_DPCUTS=true 
      -DMPIEXEC_PREFLAGS=--allow-run-as-root
    - cmake --build build --config Release -- -j8
  # artifacts:
  #   when: on_success
  #   paths:
  #     - build

# -------------------------
#  debug tests 
# -------------------------

# -----------------
# no MPI tests
# -----------------

build:no-mpi-debug:
  stage: build-debug
  rules:
    - if: $CI_COMMIT_TAG
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev ninja-build
  script:
    - cmake . -B build -G Ninja
      -DBUILD_MPI=false 
      -DBUILD_PYTHON=true 
      -DBUILD_SDDP=true 
      -DBUILD_TEST=true 
      -DBUILD_BRANCHING=false 
      -DBUILD_DPCUTS=true 
    - cmake --build build --config Debug -- -j8
  artifacts:
    when: on_success
    paths:
      - build


tests:no-mpi-debug:
  stage: test-debug
  needs:
    - build:no-mpi-debug
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - build:no-mpi-debug
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev xsltproc
  script:
    - export OMP_NUM_THREADS=2
    - cd build
    - ctest -T Test --no-compress-output || status=$? || true
    - if [ -f Testing/TAG ] ; then
    -    xsltproc $CTEST_TO_JUNIT Testing/`head -n 1 < Testing/TAG`/Test.xml > CTestResults.xml || true
    - fi
    - exit $status
  artifacts:
    when: always
    paths:
      - build/Testing/**/Test.xml
      - build/CTestResults.xml
    reports:
      junit:
        - build/CTestResults.xml


python-tests:no-mpi-debug:
  stage: test-debug
  needs:
    - build:no-mpi-debug
  rules:
    - if: $CI_COMMIT_TAG
  dependencies: 
    - build:no-mpi-debug
# ajout des paquets qui ne sont pas (encore) dans l'image de build
  before_script:
    - apt-get update && 
      apt-get -y install coinor-libclp-dev libopenmpi-dev libboost-mpi-dev
    - pip3 install nose
    - pip3 install matplotlib
    - pip3 install mpi4py
  script:
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/build/lib
    - export PYTHONPATH=$PYTHONPATH:$(pwd)/build/lib:test/python/unit:test/python/functional
    - nosetests test/python/unit       --with-xunit --xunit-file=nosetests-unit.xml       --xunit-testsuite-name=unit       || status1=$? || true
    - nosetests test/python/functional --with-xunit --xunit-file=nosetests-functional.xml --xunit-testsuite-name=functional || status2=$? || true
    - if [ $status1 -ne 0 ] || [ $status2 -ne 0 ] ; then
    -    exit 1
    - fi
  artifacts:
    when: always
    reports:
      junit:
        - nosetests-unit.xml
        - nosetests-functional.xml
