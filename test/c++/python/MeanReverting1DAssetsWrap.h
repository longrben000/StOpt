// Copyright (C) 2021 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  MEANREVERTING1DASSETSWRAP_H
#define  MEANREVERTING1DASSETSWRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "test/c++/python/FutureCurveWrap.h"
#include "test/c++/tools/simulators/MeanReverting1DAssetsSimulator.h"

typedef StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> curveType ;

/** \file MeanRevertingAndFutureWrap.h
 *  Defines the wrapping for the  Mean Reverting Simulator
 * \author Xavier Warin
 */

/// \class MeanReverting1DAssetsWrap MeanReverting1DAssetsWrap.h
/// Simulator wrap
class MeanReverting1DAssetsWrap: public MeanReverting1DAssetsSimulator< curveType >
{
private :

    std::vector< std::shared_ptr< curveType>  >  mapCurve(const std::vector< FutureCurve>  &curves)
    {
        std::vector< std::shared_ptr< curveType>  >   retVec(curves.size());
        for (int i = 0; i <  curves.size(); ++i)
        {
            retVec[i] = std::make_shared< curveType >(static_cast<curveType >(curves[i]));
        }
        return retVec;
    }

public :
    /// \brief  Constructor
    /// \param curves        future curves given
    /// \param p_sig         Volatility vector
    /// \param p_mr          Mean reverting coefficient
    /// \param p_correl      Corelation between brownians
    /// \param p_T           Maturity
    /// \param p_nbStep      Number of time step to simulate
    /// \param p_nbSimul     Number of Monte Carlo simulations
    /// \param p_bForward    is it a forward simulator (or a backward one)?
    /// \param p_nameArch    To store simulations
    /// \param p_iseed       Seed for generator
    MeanReverting1DAssetsWrap(const std::vector< FutureCurve>  &curves,
                              const std::vector<double>   &p_sig,
                              const std::vector<double>   &p_mr,
                              const Eigen::MatrixXd &p_correl,
                              const double &p_T,
                              const size_t &p_nbStep,
                              const size_t &p_nbSimul,
                              const bool &p_bForward,
                              const std::string   &p_nameArch,
                              const int  &p_iseed) :
        MeanReverting1DAssetsSimulator< curveType >(mapCurve(curves), p_sig,   p_mr, p_correl, p_T,
                p_nbStep,  p_nbSimul, p_bForward, p_nameArch, p_iseed) {}

    void resetDirection(const bool &p_bForward)
    {
        MeanReverting1DAssetsSimulator< curveType >::resetDirection(p_bForward);
    }
};

#endif /*MeanReverting1DAssetWrap.h */
