// Copyright (C) 2021 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef DYNAMICPROGRAMMINGSWITCHINGBYREGRESSION_H
#define DYNAMICPROGRAMMINGSWITCHINGBYREGRESSION_H
#include <fstream>
#include <memory>
#include <functional>
#include <Eigen/Dense>
#include "StOpt/core/grids/RegularSpaceIntGrid.h"
#include "StOpt/dp/OptimizerSwitchBase.h"
#include "StOpt/regression/BaseRegression.h"

/* \file DynamicProgrammingSwitchingByRegression.h
 * \brief Defines a simple  programm  showing how to optimize a problem by dynamic programming
 *        in the case of a pure switching problem. No stock is involved and then no interpolation is needed
 *        The deterministic state is integer.
 * \author Xavier Warin
 */

/// \brief Principal function to optimize  a problem of switching
/// \param p_grid              grid used for  deterministic integer state for each regime for switching problems
/// \param p_optimize          optimizer defining the optimisation between two time steps
/// \param p_regressor         regressor object
/// \param p_pointState        integer point state used for  at initial date
/// \param p_initialRegime     regime at initial date
/// \param p_fileToDump        file to dump continuation values
///
double  DynamicProgrammingSwitchingByRegression(const std::vector< std::shared_ptr<StOpt::RegularSpaceIntGrid> >  &p_grid,
        const std::shared_ptr<StOpt::OptimizerSwitchBase > &p_optimize,
        const std::shared_ptr<StOpt::BaseRegression> &p_regressor,
        const Eigen::ArrayXi &p_pointState,
        const int &p_initialRegime,
        const std::string   &p_fileToDump
                                               );

#endif /* DYNAMICPROGRAMMINGSWITCINGBYREGRESSION_H */
