// Copyright (C) 2021 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef DYNAMICPROGRAMMINGSWITCHINGBYREGRESSIONDIST_H
#define DYNAMICPROGRAMMINGSWITCHINGBYREGRESSIONDIST_H
#include <fstream>
#include <memory>
#include <functional>
#include <Eigen/Dense>
#include "StOpt/core/grids/RegularSpaceIntGrid.h"
#include "StOpt/dp/OptimizerSwitchBase.h"
#include "StOpt/regression/BaseRegression.h"

/* \file DynamicProgrammingByRegressionDist.h
 * \brief Defines a simple  programm  showing how to optimize a problem by dynamic programming using parallel framework and distributing for a pure switching problem with integer state
 *        calculations and data
 *        A simple grid  is used
 * \author Xavier Warin
 */

/// \brief Principal function to optimize  a problem of pure switching problem
/// \param p_grid              grid used for  deterministic integer state for each regime for switching problems
/// \param p_optimize         optimizer defining the optimisation between two time steps
/// \param p_regressor        regressor object
/// \param p_funcFinalValue   function defining the final value
/// \param p_initialPointStock     point stock used for interpolation
/// \param p_initialRegime         regime at initial date
/// \param p_fileToDump            file to dump continuation values
///
double  DynamicProgrammingSwitchingByRegressionDist(const std::vector< std::shared_ptr<StOpt::RegularSpaceIntGrid> >  &p_grid,
        const std::shared_ptr<StOpt::OptimizerSwitchBase > &p_optimize,
        const std::shared_ptr<StOpt::BaseRegression> &p_regressor,
        const Eigen::ArrayXi &p_pointState,
        const int &p_initialRegime,
        const std::string   &p_fileToDump
                                                   );

#endif /* DYNAMICPROGRAMMINGSWITCHINGBYREGRESSIONDIST_H */
