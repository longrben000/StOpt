# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
# Final value function for swings in multidimensionnal case


# final function payoff for a fictitious swing
class FinalValueFictitiousFunction:
    
    # Constructor
    def __init__(self, p_pay, p_nExerc):
        
        self.m_pay = p_pay
        self.m_nExerc = p_nExerc
        
    # final function
    # p_ireg   regime number
    # p_stock  position in the stock
    # p_state  position in the stochastic state
    def set(self, ireg, p_stock, p_state):
        
        idec = len(p_stock) * self.m_nExerc - p_stock.sum()
        
        return min(idec, len(p_stock)) * self.m_pay.apply(p_state)
