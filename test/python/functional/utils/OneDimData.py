# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
# Some data being constant per mesh in one dimensional setting
class OneDimData:
    
    # Constructor
    # p_grid  One dimensional grid
    # p_values values on the grid
    def __init__(self, p_grid, p_values):
        
        self.m_grid = p_grid
        self.m_values = p_values
        
        if (len(p_values) == p_grid.getNbStep() + 1) == False:
            pass
        
    # get the value interpolated constant per mesh at a given point
    # p_coord   the abscissa
    # return interpolated value
    def get(self, p_coord):
        
        return self.m_values[self.m_grid.getMesh(p_coord)]
