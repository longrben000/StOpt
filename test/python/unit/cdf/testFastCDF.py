# Copyright (C) 2020 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import unittest

# unit test for fast CDF on grid
################################

class testFastCDF(unittest.TestCase):
    def test2D(self):
        
        import StOptCDF
        nbSimul = 100000
        x = np.random.normal(size=(2,nbSimul));
        # y =1 for simple CDF
        y =  np.ones([nbSimul])

        # define the grid (same in both dimension)
        z1= -2 + 4*np.arange(20)/20
        z = [z1,z1]
        z = StOptCDF.fastCDF(x,z,y)
        
        

if __name__ == '__main__': 
    unittest.main()
