# Copyright (C) 2017 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import unittest
import random
import math
import StOptGrids
 
# unit test for general grids
#############################

class testGrids(unittest.TestCase):

 
    # test general grids
    def testGeneralGrids(self):
        # low value for the mesh
        lowValues =np.array([1.,2.,3.],dtype=float)
        # size of the mesh
        step = np.array([0.7,2.3,1.9],dtype=float)
        # number of step
        nbStep = np.array([4,5,6], dtype=np.int32)
        # degree of the polynomial in each direction
        degree =  np.array([2,1,3], dtype=np.int32)

        # list of mesh
        mesh1= np.array([1. + 0.7*i for  i in np.arange(5)] ,dtype=float)
        mesh2= np.array([2.+2.3*i for i in np.arange(6)],dtype=float)
        mesh3= np.array([3.+1.9*i for i in np.arange(7)],dtype=float)
        
        # create the general  grid
        grid = StOptGrids.GeneralSpaceGrid([mesh1,mesh2,mesh3] )
        
        iterGrid = grid.getGridIterator()
        # array to store
        data = np.empty(grid.getNbPoints())
        # iterates on point
        while( iterGrid.isValid()):
          #get coordinates of the point
          pointCoord = iterGrid.getCoordinate()
          data[iterGrid.getCount()] = math.log(1. + pointCoord.sum())
          iterGrid.next()
        # get back an interpolator
        ptInterp = np.array([2.3,3.2,5.9],dtype=float)
        interpol = grid.createInterpolator(ptInterp)
        # calculate interpolated value
        interpValue = interpol.apply(data)
        # test grids function
        iDim = grid.getDimension()
        pt = grid.getExtremeValues()

 
if __name__ == '__main__': 
    unittest.main()
